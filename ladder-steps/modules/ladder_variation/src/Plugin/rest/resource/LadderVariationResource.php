<?php

namespace Drupal\ladder_variation\Plugin\rest\resource;

use Drupal\Core\Database\Connection;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\rest\Plugin\ResourceBase;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Drupal\media\Entity\Media;
use Drupal\file\Entity\File;

/**
 * Provides a Ladder variation Resource.
 *
 * @RestResource(
 *   id = "ladder_variation_resource",
 *   label = @Translation("Ladder Variation"),
 *   uri_paths = {
 *     "canonical" = "/rest-api/ladder_variation/{entity_id}",
 *     "https://www.drupal.org/link-relations/create" = "/rest-api/ladder_variation"
 *   }
 * )
 */
class LadderVariationResource extends ResourceBase {

  /**
   * A current user instance.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * A current user instance.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * Constructs a Drupal\rest\Plugin\ResourceBase object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param array $serializer_formats
   *   The available serialization formats.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   A current user instance.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    array $serializer_formats,
    LoggerInterface $logger,
    AccountProxyInterface $current_user,
    Connection $connection) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);
    $this->currentUser = $current_user;
    $this->connection = $connection;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->getParameter('serializer.formats'),
      $container->get('logger.factory')->get('custom_rest'),
      $container->get('current_user'),
      $container->get('database')
    );
  }

  /**
   * Responds to entity GET requests.
   *
   * @return:
   *  Return variations of the ladder.
   */
  public function get($entity_id = NULL) {

    $arrVariations = [];
    if (!empty($entity_id)) {

      // Get variation list.
      $query = $this->connection->select('ladder_variation', 'lv');
      $query->fields('lv', ['nid', 'uid', 'variation', 'variation_id', 'created', 'updated']);
      $query->condition('lv.nid', $entity_id);
      $result = $query->execute();
      $variations = $result->fetchAll();

      foreach ($variations as $key => $variation) {
        $arrVariations[] = $variation;
      }
    }

    return new JsonResponse(['status' => 200, 'result' => $arrVariations]);
  }

  /**
   * Add new variation for the node.
   *
   * @param: $data
   *    array of data steps
   *
   * @return: Response of the API.
   */
  public function post($data) {
    // Get values.
    $uid = \Drupal::currentUser()->id();
    $nid = isset($data['nid'][0]['target_id']) ? $data['nid'][0]['target_id'] : 0;
    $variation = isset($data['variation'][0]['value']) ? $data['variation'][0]['value'] : [];
    $arrVariation = json_decode($variation);
    // echo '<pre style="color:red">'; 
    // print_r($arrVariation); 
    // echo '</pre>';
    // exit;

    // $variation_id = $arrVariation->stepId;
    $fourRandomDigit = rand(1, 1000);
    $rand_variation = $arrVariation->stepId;
    $variation_id = $rand_variation + $fourRandomDigit;

    if (\Drupal::moduleHandler()->moduleExists('ladder_image')) {
      $imageUpdate = $this->updateImage($arrVariation);
    }

    $tagUpdate = $this->updateTag($arrVariation);
    if (\Drupal::moduleHandler()->moduleExists('ladder_video')) {
      $videoUpdate = $this->updateVideo($arrVariation);
      $startTimeUpdate = $this->updateVideoStartTime($arrVariation);
      $endTimeUpdate = $this->updateVideoEndTime($arrVariation);
    }
    $variation = json_encode($arrVariation);

    \Drupal::logger('variation:uid')->notice('<pre><code>' . print_r($uid, TRUE) . '</code></pre>');
    \Drupal::logger('variation:nid')->notice('<pre><code>' . print_r($nid, TRUE) . '</code></pre>');
    // $db = \Drupal::database();
    if (!empty($nid) && !empty($uid) && is_numeric($nid) && is_numeric($uid)) {

      $result = $this->connection->insert('ladder_variation')
        ->fields([
          'nid' => $nid,
          'uid' => $uid,
          'variation' => $variation,
          'variation_id' => $variation_id,
          'created' => REQUEST_TIME,
          'updated' => REQUEST_TIME,
        ])->execute();
      return new JsonResponse(['status' => 200, 'result' => 'Variation created.']);
    }
    else {
      return new JsonResponse(['status' => 422, 'result' => 'No valid content found.']);
    }
  }

  /**
   * Update variation of tag.
   */
  public function updateTag(&$arrVariation) {

    $tags = [];
    if ($arrVariation->tags) {
      $ladderBaseService = \Drupal::service('ladder_rest.ladder.base');
      foreach ($arrVariation->tags as $tagsKey => $tagsValue) {
        $tag = (array) $tagsValue;
        if (!empty($tag)) {
          $tags_var['id'] = $ladderBaseService->getTermData($tag);
          $tags_var['name'] = $tag['name'];
          array_push($tags, $tags_var);
        }
      }
    }
    $arrVariation->tags = $tags;

    if (isset($arrVariation->children) && !empty($arrVariation->children)) {
      foreach ($arrVariation->children as $key => $currentJson) {
        $this->updateTag($currentJson);
      }
    }
  }

  /**
   * Update variation of remote video.
   */
  public function updateVideo(&$arrVariation) {

    $media = $arrVariation->videoId;
    $variationRemoteMedia = (array) $media;
    if (isset($variationRemoteMedia['is_new'])) {
      $ladderBaseService = \Drupal::service('ladder_rest.ladder.base');
      $variationRemoteMedia['id'] = $ladderBaseService->createRemoteVideo($variationRemoteMedia);
    }
    $arrVariation->videoId = $variationRemoteMedia;

    if (isset($arrVariation->children)) {
      foreach ($arrVariation->children as $key => $videoJson) {
        $this->updateVideo($videoJson);
      }
    }

  }

  /**
   * Update variation of remote video start time.
   */
  public function updateVideoStartTime(&$arrVariation) {

    $hours = $minutes = $seconds = $startingSeconds = '';
    $startTime = $arrVariation->startSeconds;

    if (!empty($startTime) && (strpos($startTime, 'NaN') === FALSE)) {
      $interval = new \DateInterval($startTime);
      $startFormatTime = $interval->format("%H:%I:%S");
      $startStrTime = preg_replace("/^([\d]{1,2})\:([\d]{2})$/", "00:$1:$2", $startFormatTime);
      sscanf($startStrTime, "%d:%d:%d", $hours, $minutes, $seconds);
      $startingSeconds = $hours * 3600 + $minutes * 60 + $seconds;
    }
    $arrVariation->startSeconds = $startingSeconds;

    if (isset($arrVariation->children)) {
      foreach ($arrVariation->children as $key => $vedioStartJson) {
        $this->updateVideoStartTime($vedioStartJson);
      }
    }
  }

  /**
   * Update variation of remote video end time.
   */
  public function updateVideoEndTime(&$arrVariation) {

    $hours = $minutes = $seconds = $endingSeconds = '';
    $endTime = $arrVariation->pauseSeconds;

    if (!empty($endTime) && (strpos($endTime, 'NaN') === FALSE)) {
      $interval = new \DateInterval($endTime);
      $endFormatTime = $interval->format("%H:%I:%S");
      $endStrTime = preg_replace("/^([\d]{1,2})\:([\d]{2})$/", "00:$1:$2", $endFormatTime);
      sscanf($endStrTime, "%d:%d:%d", $hours, $minutes, $seconds);
      $endingSeconds = $hours * 3600 + $minutes * 60 + $seconds;
    }
    $arrVariation->pauseSeconds = $endingSeconds;

    if (isset($arrVariation->children)) {
      foreach ($arrVariation->children as $key => $vedioEndJson) {
        $this->updateVideoEndTime($vedioEndJson);
      }
    }
  }

  /**
   * Update variation of image.
   */
  public function updateImage(&$arrVariation) {

    $images = [];

    if ($arrVariation->images) {
      foreach ($arrVariation->images as $imagesKey => $imagesValue) {
        $image = (array) $imagesValue;
        if (!empty($image)) {

          $image_id = $image['target_id'];
          $media = Media::load($image_id);
          $fid = $media->getSource()->getSourceFieldValue($media);
          $file = File::load($fid);
          $uri = $file->getFileUri();
          $imageUrl = file_create_url($uri);
          $name = $file->getFileName();
          $image_var['target_id'] = $image['target_id'];
          $image_var['url'] = $imageUrl;
          $image_var['name'] = $name;
          array_push($images, $image_var);
        }
      }
    }

    $arrVariation->images = $images;

    if (isset($arrVariation->children) && !empty($arrVariation->children)) {
      foreach ($arrVariation->children as $key => $imageJson) {
        $this->updateImage($imageJson);
      }
    }
  }

}
