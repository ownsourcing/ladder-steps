<?php

namespace Drupal\ladder_widget\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\node\Entity\Node;

/**
 * Class LadderWidgetController.
 *
 * @package Drupal\Core\Controller\ControllerBase
 */
class LadderWidgetController extends ControllerBase {

  /**
   * Display the ladder content.
   *
   * @return array
   *   Return markup for the ladder.
   */
  public function content($nid) {

    $build = [
      '#markup' => $this->t('No Ladder Found.'),
    ];

    // Load node.
    $node = Node::load($nid);

    // Check for node type is ladder or not.
    if (!empty($node)) {

      $bundle = $node->bundle();

      // Check if bundle is ladder.
      if ($bundle == "ladder") {

        $uid = \Drupal::currentUser()->id();
        $baseURL = \Drupal::request()->getSchemeAndHttpHost() . '/';

        // Set the div element and add the ladder react library.
        $build['#markup'] = '<div id="react-step-ladders" class="step-ladders" widget-class="ladder-widget" data-nid="' . $nid . '" data-uid="' . $uid . '"  data-url="' . $baseURL . '"></div>';

        $build['#attached']['library'] = [
          'ladder_steps/react',
        ];
      }
    }
    return $build;
  }

}
