<?php

namespace Drupal\ladder_rest;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Database\Driver\mysql\Connection;
use Drupal\node\Entity\Node;
use Symfony\Component\HttpFoundation\JsonResponse;
// Use Drupal\paragraphs\Entity\Paragraph;.
use Drupal\block_content\Entity\BlockContent;

/**
 * LadderSubscriber service class.
 */
class LadderSubscriber {

  /**
   * The database class.
   *
   * @var \Drupal\Core\Database\Database
   */
  private $database;

  /**
   * Construct function of the class.
   */
  public function __construct(Connection $database) {
    $this->database = $database;
  }

  /**
   * To add row in tree.
   *
   * @param:
   *
   * @return:
   */
  public function addRow($nid, $parentNid = 0, &$data) {

    $user_id = \Drupal::currentUser()->id();

    // Check if version exist
    // $destNid = $this->getUserNodeRevision($nid, $user_id);.
    // Update current node if revision exist
    // $nid = !empty($destNid) ? $destNid : $nid;.
    // Load node.
    $node = Node::load($nid);

    if (!empty($node)) {

      $nid = $node->id();
      $stepsValue = $node->get('field_step_er')->getValue();

      // Set node values.
      $data[$node->id() . '_' . $parentNid] = $this->nodeInfo($node, $parentNid);

      if (empty($parentNid)) {
        // Check if revision exist.
        $revisions = \Drupal::service('ladder_rest.ladder.revisions');
        // $revision = $revisions->checkRevisionExist($node->id());
        // $data[$node->id() . '_' . $parentNid]['revision_exist'] = $revision;
      }

    }
    else {
      // @TODO to add the proper error message.
      // And inspect our tree.
      return new JsonResponse('Empty values');
    }

    if (isset($stepsValue)) {
      foreach ($stepsValue as $child) {
        $this->addRow($child['target_id'], $nid, $data);
      }
    }
  }

  /**
   * To build array node of info.
   *
   * @param: node: node object
   *
   * @param: parentNid: parent node id of current no
   *
   * @return: an array of node details
   */
  public function nodeInfo($node, $parentNid = 0) {

    $user_id = \Drupal::currentUser()->id();
    $startingSeconds = $endingSeconds = $youtubeId = '';
    $data = $like = [];

    if (!empty($node)) {

      if (!$node->isPublished()) {
        return $data;
      }
      $videpData = [];
      if (\Drupal::moduleHandler()->moduleExists('ladder_video')) {
        // Set the start time.
        if (!empty($node->get('field_start_time'))) {
          $hours = $minutes = $seconds = '';
          $startTime = $node->field_start_time->value;
          if (!empty($startTime) && (strpos($startTime, 'NaN') === FALSE)) {
            $interval = new \DateInterval($startTime);
            $startFormatTime = $interval->format("%H:%I:%S");
            $startStrTime = preg_replace("/^([\d]{1,2})\:([\d]{2})$/", "00:$1:$2", $startFormatTime);
            sscanf($startStrTime, "%d:%d:%d", $hours, $minutes, $seconds);
            $startingSeconds = $hours * 3600 + $minutes * 60 + $seconds;
          }
        }

        // Set the end time.
        if (!empty($node->get('field_end_time'))) {
          $hours = $minutes = $seconds = '';
          $endTime = $node->field_end_time->value;
          if (!empty($endTime) && (strpos($endTime, 'NaN') === FALSE)) {
            $interval = new \DateInterval($endTime);
            $endFormatTime = $interval->format("%H:%I:%S");
            $endStrTime = preg_replace("/^([\d]{1,2})\:([\d]{2})$/", "00:$1:$2", $endFormatTime);
            sscanf($endStrTime, "%d:%d:%d", $hours, $minutes, $seconds);
            $endingSeconds = $hours * 3600 + $minutes * 60 + $seconds;
          }
        }

        // Set the video.
        if (!empty($node->get('field_remote_video_er'))) {
          $videoTargetId = $node->field_remote_video_er->target_id;

          $videoPath = $mediaTitle = $mediaFile = '';
          if (!empty($videoTargetId)) {
            $file = \Drupal::entityTypeManager()->getStorage("media")->load($videoTargetId);

            if (!empty($file)) {
              if (!$file->get('field_media_oembed_video_1')->isEmpty()) {
                $mediaFile = $file->get('field_media_oembed_video_1')->getString();
                preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $mediaFile, $match);
                $mediaTitle = $file->getName();
                $youtubeId = $match[1];
              }
            }
          }
          $videpData = [
            'id' => $node->field_remote_video_er->target_id,
            'title' => $mediaTitle,
            'youtubeId' => $youtubeId,
            'url' => $mediaFile,
          ];
        }
      }

      // Set the tags.
      if (!empty($node->get('field_tags_er'))) {
        $tagsData = [];
        $tags = $node->get('field_tags_er')->getValue();
        foreach ($tags as $key => $value) {
          $term = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->load($value['target_id']);
          if (isset($term)) {
            $tagsData[$key]['id'] = $value['target_id'];
            $tagsData[$key]['name'] = $term->getName();
          }
        }
      }
      // Set the body.
      if (!empty($node->get('body'))) {
        $description = $node->get('body')->getValue();
      }

      $like = [];
      // Set the vote.
      /*if(!empty($node->get('field_vote')) && FALSE) {
      $like = $node->get('field_vote')->getValue();

      // Get the users who already clicked on this particular content.
      $users = json_decode($node->field_vote->clicked_by);
      if ($users == NULL) {
      $users = new \stdClass();
      $users->default = 'default';
      }

      $clicked =  array_search($user_id, array_keys((array) $users));
      if ($clicked && isset($users->{$user_id})) {
      $like[0]['action'] = $users->{$user_id};
      }
      }*/
      // if (\Drupal::moduleHandler()->moduleExists('ladder_video')) {
      //   $vote_type = '';
      //   $voteStorage = \Drupal::entityTypeManager()->getStorage('vote');
      //   if ((bool) $voteStorage->getUserVotes($user_id, 'like', 'node', $node->id())) {
      //     $vote_type = 'like';
      //   }

      //   if ((bool) $voteStorage->getUserVotes($user_id, 'dislike', 'node', $node->id())) {
      //     $vote_type = 'dislike';
      //   }

      //   // Get like dislike votes.
      //   if (function_exists('like_and_dislike_get_votes')) {
      //     $like = like_and_dislike_get_votes($node);

      //     $likes = [
      //       'like' => $like[0],
      //       'dislike' => $like[1],
      //       'type' => $vote_type,
      //     ];
      //   }
      // }

      $codeSnippet = '';
      if (\Drupal::moduleHandler()->moduleExists('ladder_code_snippet')) {
        // Set the Code snippet.
        if (!empty($node->get('field_code_snippet'))) {
          $codeSnippet = $node->get('field_code_snippet')->getValue();
          $i = 0;
          foreach ($codeSnippet as $csPara) {
            $blockContent = BlockContent::load($csPara['target_id']);
            if (!empty($blockContent)) {
              $codeSnippet[$i] = [
                'title' => $blockContent->field_ls_title->value,
                'code_snippet' => $blockContent->field_ls_code_snippet->value,
                'help_text' => $blockContent->field_ls_help_text->value,
              ];
              $i++;
            }
          }
        }
      }

      $arrImages = [];
      if (\Drupal::moduleHandler()->moduleExists('ladder_image')) {
        // Set the vote.
        if (!empty($node->get('field_images_er'))) {
          // $images = $node->get('field_images_er')->getValue();
          $images = $node->get('field_images_er')->referencedEntities();

          foreach ($images as $key => $image) {

            if ($key > 0) {
              break;
            }

            $imageUrl = '';
            $mid = isset($image->mid) ? $image->mid->value : '';
            $name = isset($image->name) ? $image->name->value : '';
            $mediaImages = $image->get('field_media_image_1')->referencedEntities();

            foreach ($mediaImages as $mkey => $mediaImage) {
              // $imageId = isset($mediaImage->uri) ?
              // $mediaImage->uri->value : '';
              $uri = isset($mediaImage->uri) ? $mediaImage->uri->value : '';
              if (!empty($uri)) {
                $imageUrl = file_create_url($uri);
              }
            }

            $arrImages[$key] = [
              'name' => $name,
              'url' => $imageUrl,
              // 'imageId' => $imageId,
              'mid' => $mid,
            ];
          }
        }
      }

      $lecture_mode = [];
      // Get lecture mode data.
      if (!empty($node->get('field_lecture_mode_data'))) {
        $lecture_mode = Json::decode($node->field_lecture_mode_data->value);
      }

      // Score.
      $score = $node->field_score->value;

      // Last Updated.
      $last_updated = $node->changed->value;

      // Build array.
      $data = [
        'stepId' => $node->id(),
        'author' => $node->getOwner()->id(),
        'title' => $node->getTitle(),
        'videoId' => $videpData,
        'startSeconds' => $startingSeconds,
        'pauseSeconds' => $endingSeconds,
        'tags' => $tagsData,
        'parent' => $parentNid,
        'description' => $description,
        // 'likes' => $likes,
        'code_snippet' => $codeSnippet,
        'images' => $arrImages,
        'score' => $score,
        'last_updated' => $last_updated,
        'lecture_mode' => $lecture_mode,
      ];
    }

    return $data;
  }

  /**
   * To buid steps tree.
   *
   * @param: elements: node
   *
   * @param: parentId: parent if of current element
   *
   * @return: Return an array with childrens
   */
  public function buildTree(array $elements, $parentId = 0) {

    $uid = 0;
    $branch = [];

    foreach ($elements as $element) {

      if (isset($element['parent']) && $element['parent'] == $parentId) {
        $children = $this->buildTree($elements, $element['stepId']);

        // Node is reviewed or not.
        $review = $this->nodeReviewed($element['stepId'], $uid);
        $element['is_reviewed'] = $review;

        if ($review) {
          $element['is_reviewed'] = TRUE;
          $element['reviewed_id'] = $review;
        }

        if ($children) {
          $element['children'] = $children;
          $element['expanded'] = FALSE;
        }
        $branch[] = $element;
      }
    }
    return $branch;
  }

  /**
   * Check if node is read by user or not.
   *
   * @param: nid: node id
   *
   * @param: uid: user id
   *
   * @return: True if user has reviewed node
   */
  public function nodeReviewed($nid = 0, $uid = 0) {

    $isReviewed = FALSE;
    if (\Drupal::moduleHandler()->moduleExists('ladder_variation')) {
      if (!empty($nid)) {
        // Query to check if node is readed or not.
        $query = $this->database->select('flagging', 'f');
        $query->fields('f', ['id'])
          ->condition('flag_id', 'bookmark')
          ->condition('entity_type', 'node')
          ->condition('uid', $uid)
          ->condition('entity_id', $nid);
        $flagId = $query->execute()->fetchField();

        if (!empty($flagId)) {
          $isReviewed = $flagId;
        }
      }
    }
    return $isReviewed;
  }

  /**
   * To get revision of perticular node.
   *
   * @param: nid: id of the source node
   *
   * @param: uid: user id for whom need to check revision
   *
   * @return: destination node id
   */
  public function getUserNodeRevision($nid = 0, $uid = 0) {

    $dest_nid = 0;

    if (!empty($nid)) {

      // Create an object of type Select.
      $db = \Drupal::database();
      $query = $db->select('dl_default_node', 'dl');
      $query->fields('dl');
      $query->condition('dl.source_nid', $nid);
      // $query->condition('dl.uid', $uid);
      $result = $query->execute();
      $node = $result->fetchAll();

      if (!empty($node)) {
        $node = reset($node);

        if (!empty($node)) {
          $dest_nid = $node->dest_nid;
        }
      }
    }
    return $dest_nid;
  }

  /**
   * To get apated version of particular node.
   *
   * @param: nid: id of the source node
   *
   * @param: uid: user id for whom need to check revision
   *
   * @return: adapted ladder id
   */
  public function getUserAdaptedID($nid = 0, $uid = 0) {
    if (\Drupal::moduleHandler()->moduleExists('ladder_variation')) {
      $database = \Drupal::database();
      $query = $database->select('dl_adapt_variation', 'av');
      $query->fields('av', ['adapted_ladder_id']);
      $query->fields('av', ['created']);
      $query->condition('av.main_ladder_id', $nid, '=');
      $query->condition('av.uid', $uid, '=');
      $query->orderBy('av.id', 'DESC');
      $query->range(0, 1);
      $adapted_ladder_id = $query->execute()->fetchAll();
      return $adapted_ladder_id;
    }
  }

  /**
   * To get variation of particular node.
   *
   * @param: nid: id of the source node
   *
   * @param: uid: user id for whom need to check revision
   *
   * @return: variation id
   */
  public function getVariationID($nid = 0, $uid = 0) {
    if (\Drupal::moduleHandler()->moduleExists('ladder_variation')) {
      $database = \Drupal::database();
      $query = $database->select('ladder_variation', 'lv');
      $query->fields('lv', ['variation_id']);
      $query->fields('lv', ['created']);
      $query->condition('lv.nid', $nid, '=');
      $query->condition('lv.uid', $uid, '=');
      $query->orderBy('lv.id', 'DESC');
      $query->range(0, 1);
      $variation_id = $query->execute()->fetchAll();

      return $variation_id;
    }
  }

}
