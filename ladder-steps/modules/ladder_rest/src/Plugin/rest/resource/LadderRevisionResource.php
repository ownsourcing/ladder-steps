<?php

namespace Drupal\ladder_rest\Plugin\rest\resource;

use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Symfony\Component\HttpFoundation\JsonResponse;
use Drupal\node\Entity\Node;
use Drupal\Core\Session\AccountProxyInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a Ladder Revision Resource.
 *
 * @RestResource(
 *   id = "ladder_revision_resource",
 *   label = @Translation("Ladder Revisions"),
 *   uri_paths = {
 *     "canonical" = "/rest-api/ladder_revision/{entity_id}",
 *     "https://www.drupal.org/link-relations/create" = "/rest-api/ladder_revision"
 *   }
 * )
 */
class LadderRevisionResource extends ResourceBase {

  /**
   * A current user instance.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * Constructs a Drupal\rest\Plugin\ResourceBase object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param array $serializer_formats
   *   The available serialization formats.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   A current user instance.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    array $serializer_formats,
    LoggerInterface $logger,
    AccountProxyInterface $current_user) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);

    $this->currentUser = $current_user;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->getParameter('serializer.formats'),
      $container->get('logger.factory')->get('custom_rest'),
      $container->get('current_user')
    );
  }

  /**
   * Responds to entity GET requests.
   *
   * @return:
   * Return Revisions of the steps
   */
  public function get($entity_id = NULL) {

    $nodes = [];
    if (!empty($entity_id)) {

      $steps = \Drupal::service('ladder_rest.ladder.child_steps');
      $revisions = \Drupal::service('ladder_rest.ladder.revisions');
      $entity_ids = $revisions->getRevisions($entity_id);

      // Build node array.
      foreach ($entity_ids as $key => $entity_id) {

        // Build data.
        $data = [];
        $steps->addRow($entity_id, "0", $data);

        // Now turn the flat data into a multi-dimensional array.
        $nodes[$entity_id] = $steps->buildTree($data);
      }
    }

    if (!empty($nodes)) {
      return new JsonResponse($nodes); exit;
    }

    return new JsonResponse(['status_code' => 422, 'status_text' => 'no content found.']);
  }

  /**
   * Responds to POST requests.
   *
   * Returns a list of bundles for specified entity.
   *
   * @param: $data
   * array of data steps
   *
   * @return:
   *
   * API::::::::::::::::
   * Need to create Screen shots
   */
  public function post($data) {

    $uid = \Drupal::currentUser()->id();
    $nid = $data['id'];

    // Validate title field.
    if (!isset($data['title']) || empty($data['title'])) {
      $result = [
        'status_code' => 422,
        'status' => 'Title missing',
      ];
      return new ResourceResponse($result);
    }

    // Type can be LADDER_UPDATE or LADDER_ORDER_CHANGE.
    $type = isset($data['type']) ? $data['type'] : '';
    $child_ids = isset($data['child_ids']) ? $data['child_ids'] : [];
    $revisions = \Drupal::service('ladder_rest.ladder.revisions');

    // Load node.
    if (!empty($nid)) {
      $node = Node::load($nid);
    }

    /*if (!empty($node)) {

    // Get if current node author.
    $author = $node->getOwnerId();

    $new_node = $node;
    $add_revision = ($author != $uid) ? TRUE : FALSE;
    }

    Check if revision exist for the same step for the author.
    if ($add_revision) {

    Create duplicate node.
    $new_node = $node->createDuplicate();
    }

    "type":[{"target_id":"ladder"}]
    ,"title":[{"value":"iPhone — Hear what you read — Apple2"}]
    ,"field_is_ladder":[{"value":true}]
    ,"field_start_time":[{"value":"PT0H0M2S"}]
    ,"field_end_time":[{"value":"PT0H0M2S"}]
    ,"field_tags_er":[],
    "field_remote_video_er":[{"target_id":336}]*/

    // Update fields related values.
    $new_node = $this->createNode($data);

    // Manage childs if exist.
    $new_steps = [];

    foreach ($data['children'] as $key => $child) {
      $new_steps[] = $this->cloneChildSteps($child);
    }

    if (!empty($new_steps) && !empty($new_node)) {
      $new_node->set('field_step_er', $new_steps);
      $new_node->save();
    }

    // New node id.
    $new_nid = $new_node->id();

    $result = ['nid' => $new_nid];
    return new ResourceResponse($result);
  }

  /**
   * To set node field values.
   *
   * @param: new_node: ladder node object
   *
   * @param: data: posted node data
   *
   * @return:
   * Updated node array
   */
  public function createNode($data) {

    $uid = \Drupal::currentUser()->id();

    // Create new nod instead of cloning node.
    $new_node = Node::create(['type' => 'ladder']);

    $new_node->set('title', $data['title']);
    $new_node->set('field_is_revision', 1);
    $new_node->set('uid', $uid);

    // Set node referenced id.
    if (isset($data['id']) && !empty($data['id'])) {
      $new_node->set('field_referenced_ladder_er', $data['id']);
    }

    if (isset($data['description']) && !empty($data['description'])) {
      $body = [
        'value' => $data['description'],
        'format' => 'full_html',
      ];
      $new_node->set('body', $body);
    }

    if (isset($data['field_is_ladder']) && !empty($data['field_is_ladder'])) {
      $new_node->set('field_is_ladder', $data['field_is_ladder']['value']);
    }

    if (isset($data['start_time']) && !empty($data['start_time'])) {
      $new_node->set('field_start_time', $data['start_time']);
    }

    if (isset($data['end_time']) && !empty($data['end_time'])) {
      $new_node->set('field_end_time', $data['end_time']);
    }

    if (isset($data['tags']) && !empty($data['tags'])) {
      // Create new tags if needed.
      $tags = $data['tags'];
      $tag_variation = \Drupal::service('ladder_rest.ladder.base');
      foreach ($tags as $key => $tag) {
        $data['tags'][] = $tag_variation->getTermData($tag);
      }
      $new_node->set('field_tags_er', $data['tags']);
    }

    if (isset($data['remote_video']) && !empty($data['remote_video'])) {
      // Create new video entity if needed.
      $video_id = $data['remote_video']['id'];
      if (isset($data['remote_video']['is_new'])) {
        $remote_variation = \Drupal::service('ladder_rest.ladder.base');
        $video_id = $remote_variation->createRemoteVideo($data['remote_video']);
      }
      if (!empty($video_id)) {
        $new_node->set('field_remote_video_er', ["target_id" => $video_id]);
      }
    }

    // Add screenshot- must need to pass media ID only.
    if (isset($data['screen_shots']) && !empty($data['screen_shots'])) {
      // Create new screenshot entity API is called.
      $new_node->set('field_image_er', $data['screen_shots']);
    }

    // Add code snippet.
    // if (isset($data['code_snippet']) && !empty($data['code_snippet'])) {
    // // Create new code snippet entity if needed.
    //   $code_snippets = [];
    //   foreach ($data['code_snippet'] as $code_snippet) {
    //   if (!empty($code_snippet)) {.
    // $paragraph = Paragraph::create([
    //         'type' => 'code_snippet',
    //         'field_title' => [
    //           "value"  => $code_snippet['title'],
    //         ],
    //         'field_code_snippet' => [
    //           "value"  => $code_snippet['code_snippet'],
    //         ],
    //         'field_help_text' => [
    //           "value"  => $code_snippet['help_text'],
    //         ],
    //       ]);
    //       $paragraph->save();
    //       $code_snippets[] = [
    //         'target_id' => $paragraph->id(),
    //         'target_revision_id' => $paragraph->getRevisionId(),
    //       ];
    //     }
    //   }
    // if (!empty($code_snippets)) {
    //     $new_node->set('field_code_snippet', $code_snippets);
    //   }
    // }.
    $new_node->save();

    return $new_node;
  }

  /**
   * To clone child node.
   *
   * @param: parent_node: parent node
   *
   * @param: nid: current nodes
   *
   * @return: nid of cloned node
   */
  public function cloneChildSteps($data = []) {

    /*Load node.
    $node = Node::load($nid);

    Clone node.
    $new_node = $this->cloneNode($node);*/
    $new_node = $this->createNode($data);

    if (!empty($new_node)) {
      /* $steps = $node->get('field_step_er')->getValue();

      Set child node data.
      $this->setNodeValues($new_node, $data);*/

      if (!empty($data['children'])) {
        foreach ($data['children'] as $child) {
          $new_steps[] = $this->cloneChildSteps($child);
        }
      }

      if (!empty($new_steps)) {
        $new_node->set('field_step_er', $new_steps);
      }
      $new_node->save();

      return $new_node->id();
    }
  }

  /**
   * To clone node.
   *
   * @param: node: node
   *
   * @return: object of new node
   */
  public function cloneNode($node = []) {

    $new_node = [];
    if (!empty($node)) {

      $uid = \Drupal::currentUser()->id();

      // @todo: updated logic to create new nod instead of cloning node
      $new_node = $node->createDuplicate();
      $new_node->set('title', $node->getTitle() . ' - r');
      $new_node->set('field_is_revision', 1);
      $new_node->set('uid', $uid);

      $new_node->save();
    }

    return $new_node;
  }

}
