<?php

namespace Drupal\ladder_rest\Plugin\rest\resource;

use Drupal\rest\Plugin\ResourceBase;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Provides a Ladder Resource.
 *
 * @RestResource(
 *   id = "ladder_resource",
 *   label = @Translation("Ladder Resource"),
 *   uri_paths = {
 *     "canonical" = "/rest-api/ladder/{entity_id}"
 *   }
 * )
 */
class LadderResource extends ResourceBase {

  /**
   * Responds to entity GET requests.
   *
   * @return:
   * Array with hierarchy level steps
   */
  public function get($entity_id = NULL) {

    $rows = $entityResult = [];
    if (!empty($entity_id)) {

      // To check the ladder is the main ladder.
      $query = \Drupal::entityQuery('node');
      $query->condition('status', 1);
      $query->condition('nid', $entity_id);
      /*$query->condition('field_is_ladder', 1);*/
      $entityResult = $query->execute();
    }

    if (!empty($entityResult)) {

      // Set temp entity ID.
      $tmp_entity_id = $entity_id;

      // Get current user ID.
      $current_user = \Drupal::currentUser();
      $current_user_id = $current_user->id();

      $steps = \Drupal::service('ladder_rest.ladder.child_steps');
      $steps->addRow($entity_id, "0", $data);

      // Get adopted variation for user if any.
      $adapted_ladder_data = $steps->getUserAdaptedID($entity_id, $current_user_id);
      if (!empty($adapted_ladder_data)) {
        $adapted_ladder_id = $adapted_ladder_data[0]->adapted_ladder_id;
        $adapted_ladder_time = $adapted_ladder_data[0]->created;
      }

      $variation_data = $steps->getVariationID($entity_id, $current_user_id);
      if (!empty($variation_data)) {
        $variation_id = $variation_data[0]->variation_id;
        $variation_time = $variation_data[0]->created;
      }

      // Update entity_id of adopted variation.
      if (!empty($adapted_ladder_id) && ($variation_time < $adapted_ladder_time)) {

        $entity_id = $adapted_ladder_id;

        $database = \Drupal::database();
        $query = $database->select('ladder_variation', 'lv');
        $query->fields('lv', ['variation']);
        $query->condition('lv.variation_id', $entity_id, '=');
        // $query->condition('lv.uid', $current_user_id, '=');
        $query->orderBy('lv.id', 'DESC');
        $query->range(0, 1);
        $getVariation = $query->execute()->fetchField();

        $arrVariation = json_decode($getVariation);
        $arrRows = (array) $arrVariation;
        $revisions = \Drupal::service('ladder_rest.ladder.revisions');
        $revision = $revisions->checkRevisionExist($tmp_entity_id);
        $arrRows['revision_exist'] = $revision;

        $video_exist = $revisions->checkVideoExist($tmp_entity_id);
        $arrRows['video_show'] = $video_exist;

        $image_exist = $revisions->checkImageExist($tmp_entity_id);
        $arrRows['image_show'] = $image_exist;

        $code_snippet_exist = $revisions->checkCodeSnippetExist($tmp_entity_id);
        $arrRows['code_snippet_show'] = $code_snippet_exist;

        $rows[] = json_decode(json_encode($arrRows), TRUE);
      }
      elseif (!empty($variation_id)) {
        // Get variation for user edit.
        // Update entity_id of variation.
        // if (!empty($variation_id)) {.
        $entity_id = $variation_id;
        // Call add row once and then it's called resursivly to fetch each child.
        // $steps->addRow($entity_id, "0", $data);.
        $database = \Drupal::database();
        $query = $database->select('ladder_variation', 'lv');
        $query->fields('lv', ['variation']);
        $query->condition('lv.variation_id', $entity_id, '=');
        $query->condition('lv.uid', $current_user_id, '=');
        $query->orderBy('lv.id', 'DESC');
        $query->range(0, 1);
        $getVariation = $query->execute()->fetchField();

        $arrVariation = json_decode($getVariation);
        $arrRows = (array) $arrVariation;
        $revisions = \Drupal::service('ladder_rest.ladder.revisions');
        $revision = $revisions->checkRevisionExist($tmp_entity_id);
        $arrRows['revision_exist'] = $revision;

        $video_exist = $revisions->checkVideoExist($tmp_entity_id);
        $arrRows['video_show'] = $video_exist;

        $image_exist = $revisions->checkImageExist($tmp_entity_id);
        $arrRows['image_show'] = $image_exist;

        $code_snippet_exist = $revisions->checkCodeSnippetExist($tmp_entity_id);
        $arrRows['code_snippet_show'] = $code_snippet_exist;

        $rows[] = json_decode(json_encode($arrRows), TRUE);
        // }
      }
      else {
        if (\Drupal::moduleHandler()->moduleExists('ladder_variation')) {
          // For check revision exist or not.
          $revisions = \Drupal::service('ladder_rest.ladder.revisions');
          $revision = $revisions->checkRevisionExist($tmp_entity_id);
          $data[$entity_id . '_0']['revision_exist'] = $revision;

          $video_exist = $revisions->checkVideoExist($tmp_entity_id);
          $data[$entity_id . '_0']['video_show'] = $video_exist;

          $image_exist = $revisions->checkImageExist($tmp_entity_id);
          $data[$entity_id . '_0']['image_show'] = $image_exist;

          $code_snippet_exist = $revisions->checkCodeSnippetExist($tmp_entity_id);
          $data[$entity_id . '_0']['code_snippet_show'] = $code_snippet_exist;

          $data[$entity_id . '_0']['variation_show'] = TRUE;
        }
        $data[$entity_id . '_0']['variation_show'] = FALSE;
        // // Now turn the flat data into a multi-dimensional array.
        $rows = $steps->buildTree($data);
      }
    }

    // And inspect our tree.
    return new JsonResponse($rows);
  }

}
