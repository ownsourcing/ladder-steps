<?php

namespace Drupal\ladder_rest\Plugin\rest\resource;

use Drupal\rest\Plugin\ResourceBase;
use Drupal\Core\Session\AccountProxyInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Psr\Log\LoggerInterface;

/**
 * Provides a Ladder Adapt Resource.
 *
 * @RestResource(
 *   id = "ladder_adapt_resource",
 *   label = @Translation("Ladder Adapt"),
 *   uri_paths = {
 *     "canonical" = "/rest-api/ladder_adapt/{entity_id}",
 *     "https://www.drupal.org/link-relations/create" = "/rest-api/ladder_adapt"
 *   }
 * )
 */
class LadderAdaptResource extends ResourceBase {

  /**
   * A current user instance.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * Constructs a Drupal\rest\Plugin\ResourceBase object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param array $serializer_formats
   *   The available serialization formats.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   A current user instance.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    array $serializer_formats,
    LoggerInterface $logger,
    AccountProxyInterface $current_user) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);
    $this->currentUser = $current_user;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->getParameter('serializer.formats'),
      $container->get('logger.factory')->get('custom_rest'),
      $container->get('current_user')
    );
  }

  /**
   * Responds to POST requests.
   *
   * Returns a list of bundles for specified entity.
   *
   * @param: $data
   * array of data steps
   *
   * @return:
   *
   * API::::::::::::::::
   * Need to create Screen shots
   */
  public function post($data) {

    $uid = \Drupal::currentUser()->id();
    $nid = isset($data['id']) ? $data['id'] : '';
    $adapted_ladder_id = isset($data['adapted_ladder_id']) ? $data['adapted_ladder_id'] : 0;

    $db = \Drupal::database();
    if (!empty($nid) && !empty($adapted_ladder_id) && is_numeric($nid) && is_numeric($adapted_ladder_id)) {
      $result = $db->insert('dl_adapt_variation')
        ->fields([
          'uid' => $uid,
          'main_ladder_id' => $nid,
          'adapted_ladder_id' => $adapted_ladder_id,
          'created' => \Drupal::time()->getRequestTime(),
        ])
        ->execute();
      return new JsonResponse(['isAdopted' => TRUE]);
    }
    else {
      return new JsonResponse(['status_code' => 422, 'status_text' => 'No valid content found.']);
    }

  }

}
