# ladder steps

To render steps in page :
Page must have id `ladder-steps`

Attach Library :
{{ attach_library('ladder_steps/react') }}


# created separate sub-modules for each basic features

-This module contains sub module of field and rest API.
-This module contains all basic configuration which is needed to render a ladder.
-Ladder content type, remote video media, images media, code snippet (custom  \block),tags taxonomy.
-It also contains the related required modules which are required for the ladder.

#TO DO:

Need to patch apply of file_entity contrib module for image variation.
Patch Link:
REST File permanent : https://www.drupal.org/files/issues/file_entity_save_permanent-2756127-19.patch

Need to edit rest resource of content:(/admin/config/services/rest)
-Uncheck hal_json in Accepted request formats
-Check json in Accepted request formats
-Check cookie in Authentication providers
